# Usando Vagrant para os labs

## Obtendo e instalando o Vagrant
Para detalhes sobre a instalação e quais os requisitos para rodar o Vagrant consulte o [site oficial](https://www.vagrantup.com/downloads.html).

## Rodando o lab
Acesse o diretório com o Vagrantfile do lab e execute o comando:
```
vagrant up
```
Nos labs com mais de uma VM a inicialização de apenas uma VM pode ser feita indicado o nome definido no Vagrantfile, por exemplo:

```
vagrant up kerberos
```

## Conectando
Para acessar o shell precisamos indicar nome definido no Vagrantfile, por exemplo:

```
vagrant ssh kerberos
```

## Parando o lab
Para desligar as VMs do lab execute o comando:
```
vagrant halt
```
Nos labs com mais de uma VM o desligamento de apenas uma VM pode ser feita indicado o nome definido no Vagrantfile, por exemplo:

```
vagrant halt kerberos
```

## Destruindo o ambiente Vagrant
Parar e deletar a(s) VM(s):
```
vagrant destroy
```
