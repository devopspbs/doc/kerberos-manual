# Lab 2: Configuração do Cliente MIT Kerberos 5- Debian 9.6

> Esta laboratorio tem como pre-requisito o servidor do Lab 1: Instalação do Kerberos - Debian 9.6.
 
O Kerberos fornece apenas um método para armazenamento seguro e acesso a pares de nomes e senhas, conhecidos como **principais**. Ele não inclui nenhuma maneira de armazenar outras informações críticas de contas no estilo Unix, como números de ID de usuário, números de ID de grupos ou associações a grupos. O Kerberos está presente apenas para autenticar usuários com base em suas combinações de nome/senha e, idealmente, para permitir acesso adicional a recursos de rede com um único logon. Usando esse método, as contas locais ainda precisam ser criadas para corresponder aos nomes de usuários do Kerberos, senão elas não terão diretórios iniciais ou associações a grupos.
Para o cliente vamos precisar de uma outra maquina com debian instalado para servir como nosso cliente. 

## Configurações dos hosts
Na maquina cliente vamos adotar os seguinte valores como parâmetros:

hostname: labc
FQDN: labc.lab.local

Em para um ambiente sem DNS é necessário primeiramente configurar os arquivos **/etc/hosts**:
Primeiro devemos alterar o arquivo de host do servidor, adicionando a seguinte linha:
**labxx -> etc/hosts**

    #Informações do host client
    192.168.1.XX   labcxx.lab.local  labcxx

Em Seguinda vamos alterar o arquivo de hosts, hotname do cliente, devendo ficar parecido como os exemplos a seguir:
```
**labc - > etc/hostname**
    labc

**labc - > etc/hosts**
    127.0.0.1	localhost
    127.0.1.1	labc.lab.local	labc
    #Informações do host client
    192.168.0.XX   labk.lab.local  labk

```

## Instalação dos Pacotes 
No cliente vamos precisar instalar os seguintes pacotes:

    sudo apt-get install krb5-{config,user} libpam-krb5 openssh-server

Durante a instalação, o krb5-config terá que ser respondidas as seguintes questões:

    Padrão Kerberos versão 5 realm: LAB.LOCAL
    Servidores Kerberos do seu reino: labk.lab.local
    Servidor administrativo para o realm Kerberos: labk.lab.local

Essas configurações são salvas no arquivo de configuração do realm do Kerberos **/etc/krb5.conf**

## Host princ e keytab

No cliente  use o kadmin para criar um principal do host e um arquivo de keytab local, emitindo alguns comandos

    kadmin -p lab/admin
    kadmin: addprinc -randkey host/labc.lab.local
    kadmin: ktadd host/labc.lab.local
    kadmin: q

O parâmetro -randkey é usado criar principal de maquinas porque elas não usam senha para autenticar-se. Por padrão, o Kerberos salva suas chaves em **/etc/krb5.keytab** . Para listar as chaves neste arquivo, use o comando **klist -ke**. 

>Um arquivo principal e keytab do host deve ser criado e salvo em todas as várias máquinas clientes que fazem parte de uma região Kerberos.

## Configuração do PAM

Para esse lab a maior parte da configuração default do PAM vai ser suficiente, sendo necessária apenas o seguinte ajuste no arquivo **/etc/pam.d/common-password**, devendo as entradas estarem nesse padrão:

    password requisite					  pam_krb5.so minimum_uid=1000
    password [success=1 default=ignore]	  pam_unix.so obscure use_authtok try_first_pass sha512
    password requisite					  pam_deny.so
    password required					  pam_permit.so

Outro parâmetro importante a ser ajustado no PAM é o **pam_krb5.so minimum_uid = 1000**, **pam_krb5.so minimum_uid = 2000**, nos seguintes arquivos:

    /etc/pam.d/common-auth :

    auth [success = 2 default = ignorar] pam_krb5.so minimum_uid = 2000 

    /etc/pam.d/common-account :

    conta requerida pam_krb5.so minimum_uid = 2000
    
    /etc/pam.d/common-password :
    
    password requisite pam_krb5.so minimum_uid = 2000 
    
    /etc/pam.d/common-session :
    
    session optional pam_krb5.so minimum_uid = 2000 

    /etc/pam.d/common-session-noninteractive :
    
    session optional pam_krb5.so minimum_uid = 2000 

> Esse parametro determina a partir de qual valor de uid do usuário, o kerberos vai levar em consideção na hora da autenticação.

## Criação de novos usuários
Agora vamos criar uma conta de um novo usuário para gente usar no teste de autenticação usando kerberos, primeiramente vamos criar um usuário Unix padrão no cliente, observando para o uid dele ser maior que 2000, que foi estabelecido nos arquivos de configuração do PAM como minimum_uid usado para autenticação via PAM.
Podemos criar os usuário da seguinte forma:

    useradd -u 2000 -d /home/labteste -s /bin/bash labteste

Em seguida devemos criar o mesmo usuário no servidor kerberos para que ele possa se autenticar por ele também, mais com uma senha diferente da senha cliente:

    kadmin -p lab/admin
    kadmin: addprinc labteste
    kadmin: q

Com essas contas, é possível efetuar login (no console ou com ssh ) como **labteste** usando uma das duas senhas. Se a senha definida para o usuário local do cliente for usada, o acesso ao cliente será concedido, mais para o usuário local, caso use a senha do usuário definida no kerberos, será realizado o login e autenticação pelo kerberos, inclusive gerando o tickt do TGT. Caso o login seja feito com a senha do usuário local e depois quiser solicitar um tickt para o kerberos, deve-se usar o **kinit**.

## Teste

 - [ ] Realizar login usando usuário e senha local
 - [ ] Realizar login usando usuário e senho kerberos
 - [ ] Realizar login ssh usando usuário e senha local
 - [ ] Realizar login ssh usando usuário e senho kerberos