## Introdução
Kerberos é um sistema de autenticação de rede baseado no principal de um sistema de confiança tripla. sendo as partes são,usuário, o serviço no qual o usuário deseja autenticar-se e o servidor de autenticação. Nem todos os serviços e aplicações podem usar Kerberos mas, para aqueles que podem, ele propicia a criação de um ambiente de login único para todos os serviços.
Objetivos: Esse laboratorio tem como meta ajudar quem esta começando seus estudos sobre o kerberos, passando uma visão geral sobre os elementos que compõem o kerberos, ensinando a preparar o ambiente para o serviço, instalação e configuração dos pacotes básicos do linux responsaveis pelo serviço, 

## Visão geral

Se você é novo em Kerberos, há alguns termos que são interessante conhecer antes de configurar um servidor Kerberos. A maioria dos termos se relacionará a coisas com as quais você pode já estar familiarizado em outros ambientes:

**Principal:** são quaisquer usuários, computadores e serviços providos por servidores e que precisam ser definidos como Principals do Kerberos.
Instâncias: são usadas pelos principals de serviço e principal administrativos especiais.

**Realms:** é o reino único de controle fornecido pela instalação do Kerberos. Pense nisso como o domínio ou grupo ao qual seus hosts e usuários pertencem. A convenção dita que o reino deve estar em letras maiúsculas. Por padrão, o Ubuntu usará o domínio DNS convertido para maiúsculas (LABXX.LOCAL) como o domínio.

**Key Distribution Center:** (KDC) consiste de três partes, uma base de dados de todos os principais, o servidor de autenticação e o servidor de ticket granting. Para cada realm, deve haver pelo menos um KDC.

**Ticket Granting Ticket:** emitido pelo Servidor de Autenticação (AS), o Ticket Granting Ticket (TGT) é criptografado na senha do usuário que é conhecida apenas pelo próprio usuário e pelo KDC.

**Servidor de Ticket Granting:** (TGS) emite, sob solicitação, tickets de serviço para clientes.

**Tickets:** confirmam a identidade dos dois principais. Um principal é o usuário e o outro um serviço solicitado pelo usuário. Os Tickets estabelecem uma chave de criptografia usada para comunicação segura durante a seção de autenticação.

**Arquivos de Keytab:** são arquivos extraídos da base de dados do principal KDC e contêm a chave de criptografia para um serviço ou um host.

Para juntar as peças, um Realm tem pelo menos um KDC, preferencialmente mais para redundância, que contém um banco de dados de Principals. Quando um usuário se conecta a uma estação de trabalho configurada para autenticação Kerberos, o KDC emite um Ticket Granting Ticket (TGT). Se as credenciais fornecidas pelo usuário corresponderem, o usuário será autenticado e poderá solicitar tickets para serviços do Kerberized no Servidor de Concessão de Ticket (TGS). Os tíquetes de serviço permitem que o usuário se autentique no serviço sem inserir outro nome de usuário e senha.